package com.funcas.trainning.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * date: 16/3/28 下午12:36
 *
 * @author funcas
 * @version 1.0
 */
public class HelloWorldServlet extends HttpServlet{

    public HelloWorldServlet(){
        System.out.println("====> i am born");
    }


    @Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println("====> i am init....." );

    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        System.out.println("get method: " + name);
        System.out.println("===> doService-Get()");
//        this.initParameters();
//        resp.getWriter().print("<h1>Hello</h1>");
        Cookie cookie = new Cookie("username",name);
        cookie.setDomain("127.0.0.1");//域
        cookie.setMaxAge(60 * 60 * 24 * 365);//过期时间
        cookie.setPath("/");
        resp.addCookie(cookie);
//http://127.0.0.1/
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        System.out.println("post method: " + name);
    }


    @Override
    public void destroy() {
        super.destroy();
        System.out.println("==> i am dead,,,");
    }

    private Map para = new HashMap();

    private void initParameters(){
//        para.put("")
        // do something..
    }
}


