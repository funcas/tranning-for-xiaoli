package com.funcas.trainning.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * date: 16/3/29 下午12:34
 *
 * @author funcas
 * @version 1.0
 */
public class ScopeTestServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //过滤器 代码......

        //设置application
        getServletContext().setAttribute("app",1);
//设置session
        request.getSession().setAttribute("userId","111111");

//        request.getSession().getAttribute("userId");
//        response.getWriter().print("");
        request.setAttribute("password","123456");

        List<Map<String,String>> list = new ArrayList();
        Map<String,String> m = new HashMap();
        m.put("a","22");
        m.put("b","33");
        list.add(m);
        Map<String,String> mm = new HashMap<>();
        mm.put("a","322");
        mm.put("b","3221");
        list.add(mm);

        request.setAttribute("list",list);
        getServletContext().getRequestDispatcher("/WEB-INF/page/index.jsp").forward(request,response);
    }
}
