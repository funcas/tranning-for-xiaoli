package com.funcas.trainning.showcase.web;

import com.funcas.trainning.showcase.entity.IdEntity;
import com.funcas.trainning.showcase.entity.User;
import com.funcas.trainning.showcase.entity.UserInfo;
import com.funcas.trainning.showcase.service.UserInfoService;
import com.funcas.trainning.showcase.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * date: 16/3/31 下午12:59
 *
 * @author funcas
 * @version 1.0
 */

@Controller
@RequestMapping("/user")
public class SpringMvcFirstController extends HttpServlet {
    @Autowired
    private UserInfoService userInfoService;

//    @RequestMapping("/reg")
//    public void userRegistView(){
@RequestMapping(value="/regist")
public String regist(){
    return "/user/registUser";
}


    @RequestMapping(value="/registUser")
    public  void userInfoSave(HttpServletRequest request,HttpServletResponse respose)
    {
        UserInfo entity=new UserInfo();
       // IdEntity idEntity=new IdEntity();

        entity.setUsername(request.getParameter("username"));
        entity.setPassword(request.getParameter("password"));
        if(request.getParameter("sex").equals("男"))
        {
        entity.setSex(true);
    }
        else {
        entity.setSex(false);
    }
        entity.setAge(Integer.parseInt(request.getParameter("age")));
        entity.setHome(request.getParameter("home"));
        entity.setPosition(request.getParameter("position"));
        entity.setQq(request.getParameter("qq"));
        entity.setPhone(request.getParameter("phone"));
        entity.setEmail(request.getParameter("email"));
        entity.setId("notnull");

        List<UserInfo> userInfoList = new ArrayList<UserInfo>();
        userInfoList.add(entity);
        userInfoService.saveUserInfoBatch(userInfoList);

    }
    @RequestMapping(value="/log")
    public String login(){
        return "/user/login";
    }
    @RequestMapping(value="/index")
    public String index(){
        return "/index";
    }
    @RequestMapping(value="/login")
    public String login(HttpServletRequest request,HttpServletResponse respose)
    {
        String username= request.getParameter("username");
        String password= request.getParameter("password");
//       UserInfo userInfo=userInfoService.findByUserName(username);

        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password)){
            System.out.println("用户名密码不能为空");
            return "user/login";
        }else{
           boolean result =  userInfoService.doLogin(username,password);

            if(result){
                 System.out.print( "登录成功");
                return "redirect:/user/index";

            }else{
                System.out.println("用户名不存在，可以注册");
               return "redirect:/user/regist";
            }
        }


//        if (username.equals(userInfo.getUserName())&&passwrod.equals(userInfo.getPassWord())){
//            System.out.println("登录成功");
//
//        }
//        else {
//            System.out.println("登录失败");
//        }

    }


//    @Autowired
//    private UserService userService;
//
//    @RequestMapping(value = "/view")
//    public void getUserView(@RequestParam(value = "name",required = false) String name,
//                            Model model, User u){
//
//         u = new User();
//        u.setPasswd(passwd);
//        u.setUsername(username);
//        System.out.printf("===>" + entity.getUsername());
//
//        userService.save(u);
//        model.addAttribute("name","admin");
////        return "user/view";
//    }


    //@RequestMapping("/view1/{name}")

//    namepublic String getUserView1(@PathVariable String name,String par, Model model){
////        model.addAttribute("name",name);
////        return "user/view";
//        List<String> list = new ArrayList<>();
//        list.add("funcas");
//        list.add("xxxx");
//        Map<String,Object> map = new HashMap<>();
//        map.put("name",name);
//        map.put("age",28);
//        map.put("xxxx",list);
//        return "user/view";
//    }


}
