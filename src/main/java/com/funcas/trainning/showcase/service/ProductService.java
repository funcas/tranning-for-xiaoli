package com.funcas.trainning.showcase.service;

import com.funcas.trainning.showcase.entity.Product;
import com.funcas.trainning.showcase.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * date:  2016/3/31 20:48
 *
 * @author funcas
 * @version 1.0
 */
@Service
@Transactional
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> findAll(){
        return (List<Product>)productRepository.findAll();
    }

    public void saveProduct(Product entity){
        productRepository.save(entity);
    }

    public List<Product> findByName(String name){
        return productRepository.findByName(name);
    }

    public List<Product> findByProductCategoryName(String name){
        return productRepository.findByProductCategoryName(name);
    }

}
