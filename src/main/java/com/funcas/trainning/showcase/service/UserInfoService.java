package com.funcas.trainning.showcase.service;

import com.funcas.trainning.showcase.entity.UserInfo;
import com.funcas.trainning.showcase.repository.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 16-4-3.
 */
@Service
public class UserInfoService {
    @Autowired
    public UserInfoRepository userInfoRepository;

    //保存用户信息
    public  void saveUserInfoBatch(List<UserInfo> userInfos){
      userInfoRepository.save(userInfos);
    }


//    public UserInfo findByUserName(String username) {
//        return  userInfoRepository.findOneByUsername(username);
//    }

  //通过用户名、密码查询
    public boolean doLogin(String username,String password){
        UserInfo userInfo = userInfoRepository.findByUsernameAndPassword(username,password);
        if(userInfo != null){
            return true;
        }else{
            return false;
        }
//        return "";
    }


}
