package com.funcas.trainning.showcase.service;

import com.funcas.trainning.showcase.entity.ProductCategory;
import com.funcas.trainning.showcase.repository.ProductCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * date:  2016/3/31 21:23
 *
 * @author funcas
 * @version 1.0
 */
@Service
@Transactional
public class ProductCategoryService {

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    public List<ProductCategory> findAll(){
        return (List<ProductCategory>)productCategoryRepository.findAll();
    }
}
