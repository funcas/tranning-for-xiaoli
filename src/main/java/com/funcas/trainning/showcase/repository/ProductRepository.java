package com.funcas.trainning.showcase.repository;

import com.funcas.trainning.showcase.entity.Product;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * date:  2016/3/31 20:46
 *
 * @author funcas
 * @version 1.0
 */
public interface ProductRepository extends PagingAndSortingRepository<Product,String>,JpaSpecificationExecutor<Product>{

    public List<Product> findByName(String name);

    public List<Product> findByProductCategoryName(String name);
}
