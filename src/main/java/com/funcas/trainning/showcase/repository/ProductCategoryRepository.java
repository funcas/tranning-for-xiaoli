package com.funcas.trainning.showcase.repository;

import com.funcas.trainning.showcase.entity.ProductCategory;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * date:  2016/3/31 20:47
 *
 * @author funcas
 * @version 1.0
 */
public interface ProductCategoryRepository extends PagingAndSortingRepository<ProductCategory,String>,
        JpaSpecificationExecutor<ProductCategory>{
}
