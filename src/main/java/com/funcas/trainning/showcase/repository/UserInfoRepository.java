package com.funcas.trainning.showcase.repository;

import com.funcas.trainning.showcase.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


/**
 * Created by Administrator on 16-4-3.
 */

public interface UserInfoRepository extends PagingAndSortingRepository<UserInfo,String> ,JpaSpecificationExecutor<UserInfo>
{
    //
//    UserInfo findAll(String name);

    //public UserInfo findOneByUsername(String username);
   //通过用户名、密码查询
   @Query("from UserInfo as u  where u.username=?1 and u.password=?2")
    public UserInfo findByUsernameAndPassword(String username,String password);

//    @Query("select UserInfo u where u.sex = ?1")
 //   public List<UserInfo> findBySex(String sex);


//    @Modifying
//    @Query("update UserInfo u set u.sex=?1 where u.id=?2")
//    public void updateSth(String sex,String id);
}
