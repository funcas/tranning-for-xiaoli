package com.funcas.trainning.showcase.entity;

/**
 * date: 16/3/31 下午1:40
 *
 * @author funcas
 * @version 1.0
 */
public class User {

    private String username;
    private String passwd;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }
}
