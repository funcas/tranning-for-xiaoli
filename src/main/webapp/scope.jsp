<%@ page   contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<div>
    <h1>Application</h1>${applicationScope.app}

    <h1>Session:</h1>${sessionScope.userId}
    <!-- el 表达式 -->
    <div>${password}</div>

</div>

<br />


<c:forEach var="l" items="${list}">
    <%-- table --%>
    ${l.a} - ${l.b}<br/>
</c:forEach>


<!-- jstl 表达式 -->

</body>
</html>