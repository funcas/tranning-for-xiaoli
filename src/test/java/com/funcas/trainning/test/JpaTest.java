package com.funcas.trainning.test;

import com.funcas.trainning.showcase.entity.Product;
import com.funcas.trainning.showcase.entity.ProductCategory;
import com.funcas.trainning.showcase.service.ProductCategoryService;
import com.funcas.trainning.showcase.service.ProductService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * date:  2016/3/31 20:51
 *
 * @author funcas
 * @version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class JpaTest {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductCategoryService productCategoryService;

    @Test
    public void testSaveProduct(){
        Date d = new Date();
        Product product = new Product();
        product.setName("测试商品");
        product.setDescription("descript");
        product.setPrice(BigDecimal.valueOf(19.01));
        product.setProductSn("SN0000001111");
        product.setStore(1000L);
        product.setCreateDate(d);
        product.setModifyDate(d);
        product.setMarketPrice(BigDecimal.valueOf(20));

        Product product1 = new Product();
        product1.setName("测试商品2");
        product1.setDescription("descript");
        product1.setPrice(BigDecimal.valueOf(19.01));
        product1.setProductSn("SN0000001112");
        product1.setStore(1000L);
        product1.setCreateDate(d);
        product1.setModifyDate(d);
        product1.setMarketPrice(BigDecimal.valueOf(20));

        ProductCategory productCategory = new ProductCategory();
        productCategory.setName("类别一");
        productCategory.setSort(1);
        productCategory.setCreateDate(d);
        productCategory.setModifyDate(d);
        productCategory.addProduct(product);
        productCategory.addProduct(product1);
        product.setProductCategory(productCategory);
        product1.setProductCategory(productCategory);
        productService.saveProduct(product);

    }

    @Test
    public void testQuery(){
        List<Product> productList = productService.findAll();
        Assert.assertEquals(2,productList.size());
    }

    @Test
    public void testQueryCateogry(){
        List<ProductCategory> productCategories = productCategoryService.findAll();
        Assert.assertEquals(1,productCategories.size());
        Assert.assertEquals(2,productCategories.get(0).getProductSet().size());
    }

    @Test
    public void testFindByName(){
        productService.findByName("测试商品");
    }

    @Test
    public void testFindByCategoryName(){
        List<Product> products = productService.findByProductCategoryName("类别一");
        for(Product product : products){
            System.out.println(product.getName());
        }
    }
}
